import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginCommitPageRoutingModule } from './login-commit-routing.module';

import { LoginCommitPage } from './login-commit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginCommitPageRoutingModule
  ],
  declarations: [LoginCommitPage]
})
export class LoginCommitPageModule {}
