import { Component, OnInit } from '@angular/core';

import { ModalController} from '@ionic/angular';
import { RegisterPage } from '../pages/auth/register/register.page';
import { LoginPage } from '../pages/auth/login/login.page';
import { LoginCommitPage } from '../pages/auth/login-commit/login-commit.page';



@Component({
  selector: 'app-landing',
  templateUrl: './landing.page.html',
  styleUrls: ['./landing.page.scss'],
})
export class LandingPage implements OnInit {

  constructor(
    private modalController: ModalController,
  ) { }

  ngOnInit() {
  }

  async register() {
    const registerModal = await this.modalController.create({
      component: RegisterPage
    });
    return await registerModal.present();
  }

  async login() {
    const loginModal = await this.modalController.create({
      component: LoginPage,
    });
    return await loginModal.present();
  }
  async loginCommit() {
    const loginCommitModal = await this.modalController.create({
      component: LoginCommitPage,
    });
    return await loginCommitModal.present();
  }

}
