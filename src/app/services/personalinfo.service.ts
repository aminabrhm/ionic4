import { Injectable } from '@angular/core';
import { HttpServiceService } from "./http-service.service";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class PersonalinfoService {

  constructor(
    private httpService : HttpServiceService,
  ) { }

   public store( data : any ) {

    return this.httpService.makePost('auth/personalInfo', data, true).pipe(map(
      res => {
        localStorage.setItem( 'token', res.headers.get('Authorization') );
        return res;
      },
      error => {
        console.log("error stornig")
        localStorage.setItem( 'token', error.headers.get('Authorization') );
        return error.json();
      }
    ));
  }
}
